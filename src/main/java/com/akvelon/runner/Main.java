package com.akvelon.runner;


import com.akvelon.core.Tree;
import com.akvelon.utils.ParseUtils;
import com.akvelon.utils.PropertiesLoaderUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    private static final String TXT_FILE_PATH = "project.filePath";

    public static void main(String[] args) throws IOException {
        try (FileInputStream stream = new FileInputStream(PropertiesLoaderUtils.getProperty(TXT_FILE_PATH));
             InputStreamReader reader = new InputStreamReader(stream)) {
            Tree result = ParseUtils.BuildTree(reader);
        }
    }
}
