package com.akvelon.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class PropertiesLoaderUtils {

    private static final String PROPERTIES_FILE = "src/main/resources/project.properties";
    private static final Properties PROPERTIES;
    private static final Logger LOG = Logger.getLogger(PropertiesLoaderUtils.class.getName());

    static {
        PROPERTIES = new Properties();

        try (InputStream input = new FileInputStream(PROPERTIES_FILE)) {
            PROPERTIES.load(input);
        } catch (IOException e) {
            LOG.severe("Properties read error");
        }
    }

    public static String getProperty(String name) {
        return PROPERTIES.getProperty(name);
    }

    private PropertiesLoaderUtils() {
    }

}
