package com.akvelon.utils;


import com.akvelon.core.Tree;
import com.akvelon.core.impl.BinaryTree;
import com.akvelon.exceptions.UnsupportedFormatException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Utility class for parsing data
 *
 * @author Nikita_Cherevko
 */
public final class ParseUtils {

    private static final String EMPTY_PROPERTY_NAME = "project.emptyNode";
    private static final String SEPARATOR_PROPERTY_NAME = "project.separator";
    private static final String LINE_REGEX = "^[a-zA-Z]+,[a-zA-Z#]+,[a-zA-Z#]+$";
    private static final String INCORRECT_FORMAT_ERROR = "File contains data in incorrect format";
    private static final Logger LOG = Logger.getLogger(ParseUtils.class.getName());

    /**
     * Create {@link Tree} filled with data
     *
     * @param reader
     *          {@link InputStreamReader} stream
     *
     * @return Tree with data from stream
     * @throws IOException
     *          when reader contains incorrect data
     */
    public static Tree BuildTree(InputStreamReader reader) throws IOException {
        if (reader == null) {
            return null;
        }

        BinaryTree<String> result = new BinaryTree<>();
        List<String> data = readData(reader);
        for (String line : data) {
            List<String> values = parseLine(line);

            result.add(values.get(0), values.get(1), values.get(2));
        }

        return result;
    }

    private static List<String> readData(InputStreamReader reader) throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader rd = new BufferedReader(reader);

        String line;
        while ((line = rd.readLine()) != null) {
            if (isLineCorrect(line)) {
                result.add(line);
            } else {
                LOG.severe(INCORRECT_FORMAT_ERROR);
                throw new UnsupportedFormatException();
            }
        }

        return result;
    }

    private static List<String> parseLine(String line) {
        String[] labels = line.split(PropertiesLoaderUtils.getProperty(SEPARATOR_PROPERTY_NAME));
        List<String> result = Arrays.asList(labels);
        for (int i = 0; i < result.size(); i++) {
            if (PropertiesLoaderUtils.getProperty(EMPTY_PROPERTY_NAME).equals(result.get(i))) {
                result.set(i, null);
            }
        }

        return result;
    }

    private static boolean isLineCorrect(String line) {
        return line.matches(LINE_REGEX);
    }

    private ParseUtils() {
    }

}
