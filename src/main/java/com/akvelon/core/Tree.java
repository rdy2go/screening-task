package com.akvelon.core;

import com.akvelon.exceptions.UnsupportedFormatException;

public interface Tree<T> {

    boolean add(T value, T leftChild, T rightChild) throws UnsupportedFormatException;

    void delete();

    boolean contains(T value);

    void clear();

    BaseNode<T> get(T value);

    boolean isEmpty();

    interface BaseNode<T> {
        T getValue();
        BaseNode<T> getLeftChild();
        BaseNode<T> getRightChild();
        void setValue(T value);
        void setLeftChild(BaseNode<T> leftChild);
        void setRightChild(BaseNode<T> rightChild);
    }

}
