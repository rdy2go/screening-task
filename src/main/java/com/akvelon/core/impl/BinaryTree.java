package com.akvelon.core.impl;

import com.akvelon.core.Tree;
import com.akvelon.exceptions.UnsupportedFormatException;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of Tree.
 *
 * @param <T>
 *     type of objects which will be stored in the tree
 *
 * @author Nikita_Cherevko
 */
public class BinaryTree<T> implements Tree<T> {

    private Node<T> root;

    private Set<Node<T>> container;

    public BinaryTree() {
        this.container = new HashSet<>();
    }

    public BinaryTree(int size) {
        this.container = new HashSet<>(size);
    }

    public void delete() {
        //todo: implement deleting
    }

    public void clear() {
        root = null;
        container.clear();
    }

    @Override
    public BaseNode<T> get(T value) {
        return find(value);
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Add new element to binary tree
     *
     * @param value
     *              to be added
     *
     * @param leftChild
     *              left child value
     * @param rightChild
     *              right child value
     * @return true if element was added, otherwise - false
     * @throws UnsupportedFormatException
     *              if values are incorrect
     */
    public boolean add(T value, T leftChild, T rightChild) throws UnsupportedFormatException {
        if (root != null) {
            Node<T> el = getLinkedNode(value, leftChild, rightChild);

            if (el != null) {
                if (!root.getValue().equals(value) && root.getValue().equals(el.getValue())) {
                    createNewRootElement(value, leftChild, rightChild);
                    return true;
                } else if (root.getValue().equals(value)) {
                    updateRootChildren(leftChild, rightChild);
                    return true;
                } else if (el.getValue().equals(value)) {
                    el.setLeftChild(createChild(leftChild));
                    el.setRightChild(createChild(rightChild));
                    return true;
                }

            }
            Node<T> newNode = new Node<>(value, createChild(leftChild), createChild(rightChild));

            container.add(newNode);
        } else {
            root = new Node<>(value, new Node<>(leftChild), new Node<>(rightChild));
            container.add(root);
        }
        return false;
    }

    @Override
    public boolean contains(T value) {
        return find(value) != null;
    }

    private Node<T> createChild(T value) {
        if (value == null) {
            return null;
        }

        Node<T> result;
        Node<T> node = find(value);

        if (node != null) {
            result = node;
        } else {
            result = new Node<>(value);
            container.add(result);
        }
        return result;
    }

    private Node<T> find(T value) {
        return container.stream()
                .filter(el -> el.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }

    private Node<T> getLinkedNode(T value, T leftChild, T rightChild) throws UnsupportedFormatException {
        Node<T> root = find(value);

        Node<T> left = find(leftChild);

        Node<T> right = find(rightChild);

        if (!checkValues(root, left, right)) {
            throw new UnsupportedFormatException();
        }

        if (root != null) {
            return root;
        } else if (left != null) {
            return left;
        } else if (right != null) {
            return right;
        }

        return null;
    }

    private boolean checkValues(Node<T> root, Node<T> left, Node<T> right) {
        boolean isExisted = root != null || left != null || right != null;
        boolean isNew = root == null && left == null && right == null;

        return isExisted || isNew;
    }

    private void createNewRootElement(T value, T leftChild, T rightChild) {
        root = new Node<>(value,
                root.getValue().equals(leftChild) ? root : createChild(leftChild),
                root.getValue().equals(rightChild) ? root : createChild(rightChild));
        container.add(root);
    }

    private void updateRootChildren(T leftChild, T rightChild) {
        if (!root.getLeftChild().getValue().equals(leftChild)) {
            root.setLeftChild(new Node<>(leftChild));
        }

        if (!root.getRightChild().getValue().equals(rightChild)) {
            root.setRightChild(new Node<>(rightChild));
        }
    }

    static final class Node<T> implements BaseNode<T> {

        T value;
        BaseNode<T> leftChild;
        BaseNode<T> rightChild;

        Node(T value, Node<T> leftChild, Node<T> rightChild) {
            this.value = value;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        Node(T value) {
            this.value = value;
        }

        @Override
        public T getValue() {
            return value;
        }

        @Override
        public BaseNode<T> getLeftChild() {
            return leftChild;
        }

        @Override
        public BaseNode<T> getRightChild() {
            return rightChild;
        }

        @Override
        public void setValue(T value) {
            this.value = value;
        }

        @Override
        public void setLeftChild(BaseNode<T> leftChild) {
            this.leftChild = leftChild;
        }

        @Override
        public void setRightChild(BaseNode<T> rightChild) {

        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return value.equals(obj);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    ", leftChild=" + leftChild +
                    ", rightChild=" + rightChild +
                    '}';
        }
    }
}
