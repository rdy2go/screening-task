package com.akvelon.utils;


import com.akvelon.core.Tree;
import com.akvelon.exceptions.UnsupportedFormatException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ParseUtilsTest {

    private InputStreamReader reader;
    private static final String FILE_PATH = "src/test/resources/test.txt";
    private static final String PATH_TO_INCORRECT_FILE = "src/test/resources/incorrect.txt";

    @Before
    public void init() throws IOException {
        readFromFile(FILE_PATH);
    }

    @After
    public void reset() throws IOException {
        if (reader != null) {
            reader.close();
        }
    }

    @Test
    public void shouldNotBuildTreeWithNullValue() throws Exception {
        Assert.assertNull(ParseUtils.BuildTree(null));
    }

    @Test
    public void shouldBuildTreeCorrectly() throws Exception {
        Tree tree = ParseUtils.BuildTree(reader);
        Assert.assertFalse(tree.isEmpty());
    }

    @Test(expected = UnsupportedFormatException.class)
    public void shouldThrowExceptionIfValuesAreIncorrect() throws Exception {
        readFromFile(PATH_TO_INCORRECT_FILE);
        ParseUtils.BuildTree(reader);
    }

    private void readFromFile(String path) throws IOException {
        reader = new InputStreamReader(new FileInputStream(path));
    }
}
